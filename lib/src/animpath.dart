
part of anim;

/**
 * The AnimObj moves on the AnimPath.
 */
class AnimPath {
  AnimPathType ptype;
}


class AnimPathType {
  String name;
  int id;
  static final PATH_TYPE_LINE = new AnimPathType._(1, "line"); 
  static final PATH_TYPE_SOMETHINGELSE = new AnimPathType._(2, "somehtingelse"); 
  
  // private ctor
  AnimPathType._(this.id, this.name);
  
  static get values => [PATH_TYPE_LINE, PATH_TYPE_SOMETHINGELSE];
}

/**
 * Line based animation.
 */
class AnimPathLine extends AnimPath {
  int x1, y1;
  int x2, y2;
  AnimPathLine(this.x1, this.y1, this.x2, this.y2);
}

/**
 * Line based animation.
 */
class AnimMultiPath extends AnimPath {
  List<AnimPath> paths;
  AnimMultiPath(List<AnimPath> paths);
  void addPath(AnimPath path) => paths.add(path);
}