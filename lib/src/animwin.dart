
part of anim;

/**
 * AnimWin.
 * An AnimWin is a window that may contain AnimObj's.
 */
class AnimWin {
  List<AnimObj> animObjs;
}